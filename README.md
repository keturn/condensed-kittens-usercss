# Condensed Kittens UserCSS

More compact tables for [Kittens Game](https://bloodrizer.ru/games/kittens/).

The  font used is [Asap Condensed](https://www.omnibus-type.com/fonts/asap-condensed/), which works well in conjunction with the Asap font used in other themes.

Here's what it looks like with this style enabled, followed by the same view in the unmodified classic style for comparison.

![Compact version with right-aligned resource amounts.](https://gitlab.com/keturn/condensed-kittens-usercss/raw/master/screenshot-condensed.webp "Compact") ![Unmodified classic view of resources.](https://gitlab.com/keturn/condensed-kittens-usercss/raw/master/screenshot-classic.webp "Classic")
